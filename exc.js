/*  Nome: Patricia Oliveira Lima
    Rgm:28206568  */

const express = require('express');
const bodyParser = require('body-parser');
const exc = express();
exc.use(bodyParser.urlencoded({extended:true}));
exc.use(bodyParser.json());
const port = 8000;

exc.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

// Parte 01 

exc.get('/clientes/nome',(req,res)=>{
    let source = req.query;
    let ret = "Nome do cliente: " + source.nome;
    res.send("{"+ret+"}");
});

exc.post('/clientes',(req, res)=>{
    let headers_ = req.headers["access"];
    if(headers_ == "112233"){
        let dados = req.body;
        let ret = "-- Dados inseridos -- \nNome: " + dados.nome;
        ret+="\nSobrenome : " + dados.sobrenome;
        ret+="\nConta :" + dados.conta;
        res.send("{"+ret+"}");
    }else{
        res.send("Acesso negado.")
    }
});

// Parte 02 

exc.get ('/funcionarios/nome', (req, res) =>{
    let source = req.query;
    let ret = "Nome do funcionário: " + source.nome;
    res.send("{"+ret+"}");
});

exc.delete('/funcionarios/:nome', (req,res)=>{
    let headers_ = req.headers["access"]
    if(headers_ == "665522" ){
        let dados = req.params;
        let ret = "-- Dados Excluídos -- \n Nome: " +dados.nome;
        res.send("{"+ret+"}");
    }else{
        res.send("Acesso negado.")
    }
});

exc.put('/funcionarios', (req,res) =>{

    let headers_ = req.headers["access"]
    if(headers_ == "665522" ){
        let dados = req.body;
        let ret = "-- Dados Atualizados -- \n Nome: " + dados.nome +    
                " \nSobrenome: "+ dados.sobrenome +     
                " \n Cidade: " +dados.cidade;
        res.send("{"+ret+"}");
    }else{
        res.send("Acesso negado.")
    }
});



